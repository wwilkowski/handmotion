import { Detector } from './Detector';
import { Drawer } from './Drawer';
import { HandPositionConsumer } from './HandPositionConsumer';
import { HandPositionNetwork } from './handPositionNetwork/HandPositionNetwork';
import { HandGestureConsumer } from './HandGestureConsumer';
import { HandGestureNetwork } from './handGestureNetwork/HandGestureNetwork';
import { UILoader } from './UILoader';
import { VideoLoader } from './VideoLoader';
export class Handpose {
    constructor(container, video) {
        this.init = async (options) => {
            this.ui = new UILoader(this.container, this.video, false, options.fixed);
            this.detector = new Detector(this.video);
            const videoLoader = new VideoLoader(this.video);
            let handGestureNetwork;
            videoLoader.load();
            this.detector.loadHandposeModel();
            if (options.onGesture) {
                handGestureNetwork = new HandGestureNetwork(options.onGesture);
                const handGestureConsumer = new HandGestureConsumer(handGestureNetwork);
                this.detector.subscribe(handGestureConsumer);
            }
            if (options.onPositionChange) {
                let onChange = options.onPositionChange;
                if (handGestureNetwork && options.clearGestureOnPositionChange) {
                    onChange = (result) => {
                        handGestureNetwork.clear();
                        options.onPositionChange(result);
                    };
                }
                const handPositionNetwork = new HandPositionNetwork(onChange);
                const handPositionConsumer = new HandPositionConsumer(handPositionNetwork);
                this.detector.subscribe(handPositionConsumer);
            }
            if (options.drawer) {
                this.drawer = new Drawer(this.ui.getCanvas());
                this.detector.subscribe(this.drawer);
            }
            this.video.addEventListener('loadeddata', () => {
                this.detector.detectionCallback();
                this.detector.setDetectionFlag(true);
            });
        };
        this.container = container;
        this.video = video;
    }
}
//# sourceMappingURL=index.js.map