const fs = require('fs');

const dir = './dane';

let data = [];

const callback = () => {
    console.log('DATA length: ', data.length);
    console.log("COLLECTING DATA --- DONE");
};

fs.readdirSync(dir).map(filename => {
    try {
        const json = require(dir + '/' + filename);
        json.forEach(d => data.push(d));
    }
    catch (e) {
        console.log(e);
    }
});


const dataJson = JSON.stringify(data);
fs.writeFile('pose_data.json', dataJson, 'utf8', callback);