const fs = require('fs');

const dir = './data';
let data = [];


const callback = () => {
    console.log('DATA length: ', data.length);
};

fs.readdirSync(dir).map(filename => {
    try {
        const json = require(dir + '/' + filename);
        data.push(json);
    }
    catch (e) {
        console.log(e);
    }
});


const dataJson = JSON.stringify(data);
fs.writeFile('gestures_data.json', dataJson, 'utf8', callback);