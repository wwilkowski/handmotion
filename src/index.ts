import { Detector } from './Detector';
import { Drawer } from './Drawer';
import { HandPositionConsumer } from './HandPositionConsumer';
import { HandPositionNetwork } from './handPositionNetwork/HandPositionNetwork';
import { HandGestureConsumer } from './HandGestureConsumer';
import { HandGestureNetwork } from './handGestureNetwork/HandGestureNetwork';
import { NeuralNetworksResult } from './types';
import { UILoader } from './UILoader';
import { VideoLoader } from './VideoLoader';

interface Options {
    onPositionChange?: (result: NeuralNetworksResult) => void;
    onGesture?: (result: NeuralNetworksResult) => void;
    clearGestureOnPositionChange?: Boolean;
    fixed?: Boolean;
    drawer?: Boolean;
}


export class Handpose {
    private ui!: UILoader;
    private detector!: Detector;
    private drawer!: Drawer;
    private container: HTMLDivElement;
    private video: HTMLVideoElement;

    constructor(container: HTMLDivElement, video: HTMLVideoElement) {
        this.container = container;
        this.video = video;
    }

    init = async (options: Options) => {
        this.ui = new UILoader(this.container, this.video, false, options.fixed);
        this.detector = new Detector(this.video);

        const videoLoader = new VideoLoader(this.video);
        let handGestureNetwork: HandGestureNetwork | null;
        videoLoader.load();
        this.detector.loadHandposeModel();

        if (options.onGesture) {
            handGestureNetwork = new HandGestureNetwork(options.onGesture);
            const handGestureConsumer = new HandGestureConsumer(handGestureNetwork);
            this.detector.subscribe(handGestureConsumer);
        }

        if (options.onPositionChange) {
            let onChange = options.onPositionChange;
            if (handGestureNetwork && options.clearGestureOnPositionChange) {
                onChange = (result: NeuralNetworksResult) => {
                    handGestureNetwork.clear();
                    options.onPositionChange(result);
                };
            }
            const handPositionNetwork = new HandPositionNetwork(onChange);
            const handPositionConsumer = new HandPositionConsumer(handPositionNetwork);
            this.detector.subscribe(handPositionConsumer);
        }

        if (options.drawer) {
            this.drawer = new Drawer(this.ui.getCanvas());
            this.detector.subscribe(this.drawer);
        }

        this.video.addEventListener('loadeddata', () => {
            this.detector.detectionCallback();
            this.detector.setDetectionFlag(true);
        });
    };
}



