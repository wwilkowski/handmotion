import { blurredVideoStyle, canvasStyle, containerStyle, videoStyle, fixedContainerStyle } from './constants/styles';
import { setElementAttribute } from './helpers';

export class UILoader {
    private video: HTMLVideoElement;
    private canvas: HTMLCanvasElement;
    private width: number = 640;
    private height: number = 480;
    private blurred: Boolean;
    private fixed: Boolean;

    constructor(container: HTMLDivElement, video: HTMLVideoElement, blurred: Boolean = true, fixed: Boolean = false) {
        this.video = video;
        this.blurred = blurred;
        this.fixed = fixed;
        this.initVideoProperties();

        this.canvas = this.createCanvasElement();
        this.appendCanvasToDocument(container, this.canvas);
    }

    initVideoProperties = () => {
        setElementAttribute(this.video, 'width', `${this.width}`);
        setElementAttribute(this.video, 'height', `${this.height}`);
        setElementAttribute(this.video, 'style', this.blurred ? blurredVideoStyle : videoStyle);
    };

    createCanvasElement = () => {
        const canvas = document.createElement('canvas');

        setElementAttribute(canvas, 'width', `${this.width}`);
        setElementAttribute(canvas, 'height', `${this.height}`);
        setElementAttribute(canvas, 'style', canvasStyle);

        return canvas;
    };

    appendCanvasToDocument = (container: HTMLDivElement, canvas: HTMLCanvasElement) => {
        setElementAttribute(container, 'style', this.fixed ? fixedContainerStyle : containerStyle);

        container.appendChild(canvas);
    };

    getCanvas = () => this.canvas;
}