import { AbstractDetectorSubscriber } from "./AbstractDetectorSubscriber";
export class HandGestureConsumer extends AbstractDetectorSubscriber {
    constructor(handGestureNetwork) {
        super();
        this.receiveData = async (data) => {
            const { pinky, thumb, palmBase, middleFinger } = data.annotations;
            const pinkyX = pinky[0][0];
            const pinkyY = pinky[0][1];
            const thumbX = thumb[0][0];
            const thumbY = thumb[0][1];
            const palmBaseY = palmBase[0][1];
            const middleY = middleFinger[0][1];
            const centerX = (pinkyX + thumbX) / 2;
            const centerY = (pinkyY + thumbY) / 2;
            const verticalDistance = Math.abs(palmBaseY - middleY);
            await this.handGestureNetwork.predict([centerX, centerY, verticalDistance]);
        };
        this.noData = () => { };
        this.handGestureNetwork = handGestureNetwork;
    }
}
//# sourceMappingURL=HandGestureConsumer.js.map