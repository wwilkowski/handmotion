import gestureWorker from "./gestureWorker";
class HandGestureNetwork {
    constructor(callback) {
        this.predict = async (input) => {
            if (!this.pending) {
                this.pending = true;
                this.worker.postMessage({
                    input: input
                });
            }
        };
        this.clear = () => {
            this.worker.postMessage({
                clear: true
            });
        };
        this.worker = new Worker(gestureWorker);
        this.worker.onmessage = (event) => {
            if (event.data && event.data.log) {
                console.log(event.data.log);
            }
            else {
                this.pending = false;
                if (event.data) {
                    callback(event.data);
                }
            }
        };
        this.pending = false;
    }
}
export { HandGestureNetwork };
//# sourceMappingURL=HandGestureNetwork.js.map