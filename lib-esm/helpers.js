export const setElementAttribute = (element, attribute, value) => {
    element.setAttribute(attribute, value);
};
export const smoothData = (values, severity) => {
    let newValues = [];
    newValues.push(values[0]);
    for (let i = 1; i < values.length; i++) {
        const start = (i - severity) > 0 ? i - severity : 0;
        const end = (i + severity) < values.length ? i + severity : values.length;
        let sumX = 0;
        let sumY = 0;
        for (let j = start; j < end; j++) {
            sumX += values[j][0];
            sumY += values[j][1];
        }
        const avgX = sumX / (end - start);
        const avgY = sumY / (end - start);
        newValues.push([avgX, avgY, values[i][2], values[i][3]]);
    }
    return newValues;
};
export const filterData = (values, minDistance) => {
    let newValues = [];
    newValues.push(values[0]);
    let lastPoint = values[0];
    for (let i = 1; i < values.length; i++) {
        const p = values[i];
        const distanceXToNextPoint = Math.abs(p[0] - lastPoint[0]);
        const distanceYToNextPoint = Math.abs(p[1] - lastPoint[1]);
        const horizontalDistance = Math.abs(p[2] - lastPoint[2]);
        const verticalDistance = Math.abs(p[3] - lastPoint[3]);
        if (distanceXToNextPoint >= minDistance || distanceYToNextPoint >= minDistance || horizontalDistance > 10 || verticalDistance > 10) {
            newValues.push(p);
            lastPoint = p;
        }
    }
    return newValues;
};
export const normalizeData = (values) => {
    let maxX = 0;
    let maxY = 0;
    let maxHorizontalDistance = 0;
    let maxVerticalDistance = 0;
    for (let j = 0; j < values.length; j++) {
        if (maxX < values[j][0]) {
            maxX = values[j][0];
        }
        if (maxY < values[j][1]) {
            maxY = values[j][1];
        }
        if (maxHorizontalDistance < values[j][2]) {
            maxHorizontalDistance = values[j][2];
        }
        if (maxVerticalDistance < values[j][3]) {
            maxVerticalDistance = values[j][3];
        }
    }
    return values.map(value => [value[0] / maxX, value[1] / maxY, value[2] / maxHorizontalDistance, value[3] / maxVerticalDistance]);
};
export const checkPointsDistance = (lastPoint, newPoint) => {
    if (!lastPoint) {
        return true;
    }
    const distanceXToNextPoint = Math.abs(newPoint[0] - lastPoint[0]);
    const distanceYToNextPoint = Math.abs(newPoint[1] - lastPoint[1]);
    const horizontalDistance = Math.abs(newPoint[2] - lastPoint[2]);
    const verticalDistance = Math.abs(newPoint[3] - lastPoint[3]);
    if (distanceXToNextPoint >= 15 || distanceYToNextPoint >= 15 || horizontalDistance > 10 || verticalDistance > 10) {
        return true;
    }
    return false;
};
//# sourceMappingURL=helpers.js.map