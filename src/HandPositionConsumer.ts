import { AbstractDetectorSubscriber } from "./AbstractDetectorSubscriber";
import { HandPositionNetwork } from "./handPositionNetwork/HandPositionNetwork";
import { Hand } from './types';

const pointsPairs: Array<Array<number>> = [[0, 2, 4], [0, 6, 8], [0, 10, 12], [0, 14, 16], [0, 18, 20]];

export class HandPositionConsumer extends AbstractDetectorSubscriber {
    private handPositionNetwork: HandPositionNetwork;

    constructor(handPositionNetwork: HandPositionNetwork) {
        super();
        this.handPositionNetwork = handPositionNetwork;
    }

    receiveData = async (data: Hand) => {
        const { landmarks } = data;
        const angles: Array<number> = [];

        pointsPairs.forEach((point: Array<number>) => {
            const a = [landmarks[point[1]][0] - landmarks[point[0]][0], landmarks[point[1]][1] - landmarks[point[0]][1]];
            const b = [landmarks[point[2]][0] - landmarks[point[1]][0], landmarks[point[2]][1] - landmarks[point[1]][1]];

            angles.push((a[0] * b[0] + a[1] * b[1]) / (Math.sqrt(a[0] ** 2 + a[1] ** 2) * (Math.sqrt(b[0] ** 2 + b[1] ** 2))));
        });

        await this.handPositionNetwork.predict(angles);
    };

    noData = () => { };
}

