import * as handpose from '@tensorflow-models/handpose';
import * as tf from '@tensorflow/tfjs';
import '@tensorflow/tfjs-backend-webgl';
import { Logger } from './Logger';
tf.ENV.set("WEBGL_CPU_FORWARD", true);
class Detector {
    constructor(video) {
        this.subscribers = [];
        this.shouldDetect = false;
        this.loadHandposeModel = async () => {
            this.model = await handpose.load({
                detectionConfidence: 0.97
            });
            Logger.log('Handpose model has been loaded');
        };
        this.detectFromFrame = async () => {
            const result = await this.model.estimateHands(this.video);
            if (result && result.length) {
                this.subscribers.forEach(s => s.receiveData(result[0]));
            }
        };
        this.setDetectionFlag = (value) => {
            this.shouldDetect = value;
        };
        this.detectionCallback = async () => {
            if (this.model && this.shouldDetect) {
                const result = await this.model.estimateHands(this.video);
                if (result && result.length) {
                    this.subscribers.forEach(s => s.receiveData(result[0]));
                }
                else {
                    this.subscribers.forEach(s => s.noData());
                }
            }
            requestAnimationFrame(this.detectionCallback);
        };
        this.subscribe = (subscriber) => this.subscribers.push(subscriber);
        this.video = video;
    }
}
export { Detector };
//# sourceMappingURL=Detector.js.map