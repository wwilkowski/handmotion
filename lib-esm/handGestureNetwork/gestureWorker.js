export default URL.createObjectURL(new Blob([
    '(',
    function () {
        const smoothData = (values, severity) => {
            let newValues = [];
            newValues.push(values[0]);
            for (let i = 1; i < values.length; i++) {
                const start = (i - severity) > 0 ? i - severity : 0;
                const end = (i + severity) < values.length ? i + severity : values.length;
                let sumX = 0;
                let sumY = 0;
                for (let j = start; j < end; j++) {
                    sumX += values[j][0];
                    sumY += values[j][1];
                }
                const avgX = sumX / (end - start);
                const avgY = sumY / (end - start);
                newValues.push([avgX, avgY, values[i][2]]);
            }
            return newValues;
        };
        const normalizeData = (values) => {
            let maxX = 0;
            let maxY = 0;
            let maxVerticalDistance = 0;
            for (let j = 0; j < values.length; j++) {
                if (maxX < values[j][0]) {
                    maxX = values[j][0];
                }
                if (maxY < values[j][1]) {
                    maxY = values[j][1];
                }
                if (maxVerticalDistance < values[j][2]) {
                    maxVerticalDistance = values[j][2];
                }
            }
            return values.map(value => [value[0] / maxX, value[1] / maxY, value[2] / maxVerticalDistance]);
        };
        const checkPointsDistance = (lastPoint, newPoint) => {
            if (!lastPoint) {
                return true;
            }
            const distanceXToNextPoint = Math.abs(newPoint[0] - lastPoint[0]);
            const distanceYToNextPoint = Math.abs(newPoint[1] - lastPoint[1]);
            const verticalDistance = Math.abs(newPoint[2] - lastPoint[2]);
            if (distanceXToNextPoint >= 15 || distanceYToNextPoint >= 15 || verticalDistance > 10) {
                return true;
            }
            return false;
        };
        importScripts("https://cdn.jsdelivr.net/npm/@tensorflow/tfjs");
        const ctx = self;
        let queue = [];
        let model;
        let guide = false;
        const loadModel = async () => {
            model = await tf.loadLayersModel('http://wwilkowski.com/tfjsmodel2/model.json');
            ctx.postMessage({
                'log': `Model loaded`
            });
        };
        loadModel();
        let timeout;
        let lastInput;
        ctx.addEventListener("message", (event) => {
            if (event.data.clear) {
                queue = [];
                return;
            }
            const { input } = event.data;
            if (checkPointsDistance(lastInput, input)) {
                if (queue.length > 15) {
                    queue.shift();
                }
                clearTimeout(timeout);
                lastInput = input;
                if (!guide) {
                    queue.push(input);
                }
            }
            if (queue.length >= 3 && !guide && model) {
                const finalQueue = normalizeData(smoothData(queue, 2));
                let testTensor = tf.tensor(finalQueue);
                testTensor = testTensor.reshape([1, testTensor.shape[0], testTensor.shape[1]]);
                const prediction = model.predict(testTensor);
                const winnerIndex = Array.from(prediction.argMax(-1).dataSync())[0];
                const probability = Array.from(prediction.dataSync())[winnerIndex];
                ctx.postMessage({
                    'log': `${winnerIndex} : ${probability}`
                });
                if (probability >= 0.9) {
                    queue = [];
                    ctx.postMessage({
                        'pose': winnerIndex,
                        'probability': probability
                    });
                    guide = true;
                    setTimeout(() => {
                        guide = false;
                        ctx.postMessage({
                            'log': 'guide po 500'
                        });
                    }, 500);
                }
                else {
                    ctx.postMessage(null);
                }
            }
            else {
                ctx.postMessage(null);
            }
            timeout = setTimeout(() => {
                queue = [];
                ctx.postMessage({
                    'log': 'clear po 1000'
                });
            }, 1000);
        });
    }.toString(),
    ')()',
], { type: 'application/javascript' }));
//# sourceMappingURL=gestureWorker.js.map