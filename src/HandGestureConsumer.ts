import { AbstractDetectorSubscriber } from "./AbstractDetectorSubscriber";
import { HandGestureNetwork } from "./handGestureNetwork/HandGestureNetwork";
import { Hand, HandPoseResult } from './types';

export class HandGestureConsumer extends AbstractDetectorSubscriber {
    private handGestureNetwork: HandGestureNetwork;

    constructor(handGestureNetwork: HandGestureNetwork) {
        super();
        this.handGestureNetwork = handGestureNetwork;
    }

    receiveData = async (data: Hand) => {
        const { pinky, thumb, palmBase, middleFinger } = data.annotations;
        const pinkyX = pinky[0][0];
        const pinkyY = pinky[0][1];
        const thumbX = thumb[0][0];
        const thumbY = thumb[0][1];
        const palmBaseY = palmBase[0][1];
        const middleY = middleFinger[0][1];

        const centerX = (pinkyX + thumbX) / 2;
        const centerY = (pinkyY + thumbY) / 2;
        const verticalDistance = Math.abs(palmBaseY - middleY);

        await this.handGestureNetwork.predict([centerX, centerY, verticalDistance]);
    };

    noData = () => { };
}

