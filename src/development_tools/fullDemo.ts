import { Detector } from '../Detector';
import { Drawer } from '../Drawer';
import { HandGestureConsumer } from '../HandGestureConsumer';
import { HandGestureNetwork } from '../handGestureNetwork/HandGestureNetwork';
import { HandPositionConsumer } from '../HandPositionConsumer';
import { HandPositionNetwork } from '../handPositionNetwork/HandPositionNetwork';
import { NeuralNetworksResult } from '../types';
import { UILoader } from '../UILoader';
import { VideoLoader } from '../VideoLoader';

const updateList = (gesture: string) => {
    if (list.getElementsByTagName("li").length > 10) {
        if (list.lastElementChild) {
            list.removeChild(list.lastElementChild);
        }
    }
    const listElement = document.createElement('li');
    listElement.textContent = gesture;
    list.insertBefore(listElement, list.firstElementChild);
};

const onHandGesture = (value: NeuralNetworksResult) => {
    const labels = ['LEWO', 'PRAWO', 'GÓRA', 'DÓŁ', "PRZYBLIŻENIE", "ODDALENIE"];
    const labels2 = ['Closed hand', 'One finger', 'Two fingers', 'Three fingers', "Four fingers", "Opened hand"];
    if (value && handPosition) {
        if (value.probability > 0.9) {
            handGestureInfo.textContent = `${labels[value.pose]}`;
            updateList(`Ruch w ${labels[value.pose]} w pozycji ${labels2[handPosition]}`);
        } else {
            handGestureInfo.textContent = `null`;
        }
    }
};

const container = document.querySelector('.videoContainer') as HTMLDivElement;
const video = document.querySelector('.video') as HTMLVideoElement;
const trainButton = document.querySelector('.trainButton');
const handPositionInfo = document.querySelector('.handPositionInfo') as HTMLSpanElement;
const handGestureInfo = document.querySelector('.handGestureInfo') as HTMLSpanElement;

let handPosition: number | null = null;

const videoLoader = new VideoLoader(video);
videoLoader.load();

const onHandPositionChange = () => {
    console.log("Position has been changed");
};

const onHandPosition = (value: NeuralNetworksResult) => {
    const labels = ['Closed hand', 'One finger', 'Two fingers', 'Three fingers', "Four fingers", "Opened hand"];

    if (value && value.probability >= 0.9) {
        if (!handPosition) {
            handPosition = value.pose;
            handPositionInfo.textContent = labels[value.pose];
        }
        else {
            if (handPosition !== value.pose) {
                onHandPositionChange();
                handPosition = value.pose;
                handPositionInfo.textContent = labels[value.pose];
            }
        }
    }
};

const ui = new UILoader(container, video);
const detector = new Detector(video);
const drawer = new Drawer(ui.getCanvas());
const handPositionNetwork = new HandPositionNetwork(onHandPosition);
const handPositionConsumer = new HandPositionConsumer(handPositionNetwork);

const handGestureNetwork = new HandGestureNetwork(onHandGesture);
const handGestureConsumer = new HandGestureConsumer(handGestureNetwork);
const list = document.querySelector('.gestureList') as HTMLUListElement;

detector.loadHandposeModel();
detector.subscribe(handPositionConsumer);
detector.subscribe(handGestureConsumer);
video.addEventListener('loadeddata', () => {
    detector.detectionCallback();
});

trainButton?.addEventListener('click', async () => {
    detector.setDetectionFlag(true);
});