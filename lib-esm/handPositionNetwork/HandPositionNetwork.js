import poseWorker from "./poseWorker";
class HandPositionNetwork {
    constructor(callback) {
        this.predict = async (landmarks) => {
            if (!this.pending) {
                this.pending = true;
                this.worker.postMessage({
                    input: landmarks
                });
            }
        };
        this.worker = new Worker(poseWorker);
        this.lastPosition = null;
        this.worker.onmessage = (event) => {
            if (event.data && event.data.log) {
                console.log(event.data.log);
            }
            else {
                this.pending = false;
                if (event.data) {
                    if (!this.lastPosition || event.data.pose !== this.lastPosition) {
                        this.lastPosition = event.data.pose;
                        callback(event.data);
                    }
                }
            }
        };
        this.pending = false;
    }
}
export { HandPositionNetwork };
//# sourceMappingURL=HandPositionNetwork.js.map