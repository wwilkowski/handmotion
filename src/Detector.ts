import * as handpose from '@tensorflow-models/handpose';
import * as tf from '@tensorflow/tfjs';
import '@tensorflow/tfjs-backend-webgl';
import { AbstractDetectorSubscriber } from './AbstractDetectorSubscriber';
import { Logger } from './Logger';

tf.ENV.set("WEBGL_CPU_FORWARD", true);

class Detector {
    private video: HTMLVideoElement;
    private subscribers: AbstractDetectorSubscriber[] = [];
    private shouldDetect: Boolean = false;
    private model: any;

    constructor(video: HTMLVideoElement) {
        this.video = video;
    }

    loadHandposeModel = async () => {
        this.model = await handpose.load({
            detectionConfidence: 0.97
        });

        Logger.log('Handpose model has been loaded');
    };

    detectFromFrame = async () => {
        const result = await this.model.estimateHands(this.video);
        if (result && result.length) {
            this.subscribers.forEach(s => s.receiveData(result[0]));
        }
    };

    setDetectionFlag = (value: Boolean) => {
        this.shouldDetect = value;
    };

    detectionCallback = async () => {
        if (this.model && this.shouldDetect) {
            const result = await this.model.estimateHands(this.video);
            if (result && result.length) {
                this.subscribers.forEach(s => s.receiveData(result[0]));
            }
            else {
                this.subscribers.forEach(s => s.noData());
            }
        }
        requestAnimationFrame(this.detectionCallback);
    };

    subscribe = (subscriber: AbstractDetectorSubscriber) => this.subscribers.push(subscriber);
}

export { Detector };
