import { Detector } from '../Detector';
import { Drawer } from '../Drawer';
import { HandGestureConsumer } from '../HandGestureConsumer';
import { HandGestureNetwork } from '../handGestureNetwork/HandGestureNetwork';
import { NeuralNetworksResult } from '../types';
import { UILoader } from '../UILoader';
import { VideoLoader } from '../VideoLoader';

const updateList = (gesture: string) => {
    if (list.getElementsByTagName("li").length > 10) {
        if (list.lastElementChild) {
            list.removeChild(list.lastElementChild);
        }
    }
    const listElement = document.createElement('li');
    listElement.textContent = gesture;
    list.insertBefore(listElement, list.firstElementChild);
};

const setChart = (value: NeuralNetworksResult) => {
    const labels = ['LEWO', 'PRAWO', 'GÓRA', 'DÓŁ', "PRZYBLIŻENIE", "ODDALENIE"];
    if (value && gesture) {
        if (value.probability > 0.9) {
            updateList(`Gest ${counter++}: ${labels[value.pose]}`);
        }
    }
};

const container = document.querySelector('.videoContainer') as HTMLDivElement;
const video = document.querySelector('.video') as HTMLVideoElement;
const trainButton = document.querySelector('.trainButton');
let counter = 0;

const videoLoader = new VideoLoader(video);
videoLoader.load();

const ui = new UILoader(container, video);
const detector = new Detector(video);
const drawer = new Drawer(ui.getCanvas());
const handGesture = new HandGestureNetwork(setChart);
const handGestureConsumer = new HandGestureConsumer(handGesture);
const gesture = document.querySelector('.gesture');
const list = document.querySelector('.gestureList') as HTMLUListElement;

detector.loadHandposeModel();
detector.subscribe(handGestureConsumer);
video.addEventListener('loadeddata', () => {
    detector.detectionCallback();
});

trainButton?.addEventListener('click', async () => {
    detector.setDetectionFlag(true);
});