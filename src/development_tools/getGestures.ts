import { UILoader } from '../UILoader';
import { Detector } from '../Detector';
import { Drawer } from '../Drawer';
import { VideoLoader } from '../VideoLoader';
import { AbstractDetectorSubscriber } from '../AbstractDetectorSubscriber';
import { Hand } from '../types';
import Chart from 'chart.js';

interface FileData {
    "ys": number,
    "xs": Array<number[]>;
}

class DataGetter extends AbstractDetectorSubscriber {
    private trainingData: Array<number[]>;
    private detectionFlag: Boolean;
    private finalData: Array<FileData>;

    constructor() {
        super();

        this.trainingData = [];
        this.detectionFlag = false;
        this.finalData = [];
    }

    receiveData(data: Hand): void {
        if (this.detectionFlag) {
            const { pinky, thumb, palmBase, middleFinger } = data.annotations;
            const pinkyX = pinky[0][0];
            const pinkyY = pinky[0][1];
            const thumbX = thumb[0][0];
            const thumbY = thumb[0][1];
            const palmBaseY = palmBase[0][1];
            const middleY = middleFinger[0][1];

            const centerX = (pinkyX + thumbX) / 2;
            const centerY = (pinkyY + thumbY) / 2;
            const horizontalDistance = Math.abs(pinkyX - thumbX);
            const verticalDistance = Math.abs(palmBaseY - middleY);
            this.trainingData.push([centerX, centerY, horizontalDistance, verticalDistance]);

            points.textContent = `${this.trainingData.length}`;
        }
    }

    noData(): void {
    }

    setFlag = (flag: Boolean) => {
        this.detectionFlag = flag;
    };

    getTrainingData = () => this.trainingData;

    clearTrainingData = () => this.trainingData = [];

    pushToData = () => {
        const poseIndex = parseInt((document.querySelector('.trainingPoseIndex') as HTMLInputElement).value);
        const smoothedData = smoothData(this.trainingData, 2);
        const filteredData = filterData(smoothedData, 10);

        this.finalData.push({
            ys: poseIndex,
            xs: filteredData
        });

        const collectedData = document.querySelector('.collectedData') as HTMLSpanElement;
        collectedData.textContent = `${this.finalData.length} sequences`;

        this.clearTrainingData();
    };

    getFinalData = () => this.finalData;

    showData = () => {
        createChart(this.trainingData);
    };
}

const container = document.querySelector('.videoContainer') as HTMLDivElement;
const video = document.querySelector('.video') as HTMLVideoElement;
const saveDataButton = document.querySelector('.saveDataButton') as HTMLButtonElement;
const pushToDataButton = document.querySelector('.pushToDataButton') as HTMLButtonElement;
const points = document.querySelector('.points') as HTMLSpanElement;
const ctx = (document.getElementById('myChart') as HTMLCanvasElement).getContext('2d') as CanvasRenderingContext2D;

const videoLoader = new VideoLoader(video);
videoLoader.load();

const ui = new UILoader(container, video);
const detector = new Detector(video);
const drawer = new Drawer(ui.getCanvas());
const dataGetter = new DataGetter();

detector.loadHandposeModel();
detector.subscribe(drawer);
detector.subscribe(dataGetter);

video.addEventListener('loadeddata', () => {
    detector.detectionCallback();
    detector.setDetectionFlag(true);
});

let pressed = false;

document.addEventListener('keypress', function (event) {
    const key = event.key || event.keyCode;
    if (key === ' ' && pressed === false) {
        pressed = true;
        dataGetter.clearTrainingData();
        dataGetter.setFlag(true);
    }
    if (key === 'b') {
        dataGetter.pushToData();
    }
});

document.addEventListener('keyup', function (event) {
    const key = event.key || event.keyCode;
    if (key === ' ' && pressed === true) {
        pressed = false;
        dataGetter.setFlag(false);
        dataGetter.showData();
    }
});

pushToDataButton.addEventListener('click', () => {
    dataGetter.pushToData();
});


saveDataButton.addEventListener('click', () => {
    const finalData = dataGetter.getFinalData();
    const json = JSON.stringify(finalData);
    const blob = new Blob([json], { type: 'application/json' });
    const url = URL.createObjectURL(blob);

    const a = document.createElement('a');
    a.download = 'gestures_data.json';
    a.href = url;

    a.click();
});

const filterData = (values: Array<number[]>, minDistance: number) => {
    let newValues = [];
    newValues.push(values[0]);
    let lastPoint = values[0];

    for (let i = 1; i < values.length; i++) {
        const p = values[i];
        const distanceXToNextPoint = Math.abs(p[0] - lastPoint[0]);
        const distanceYToNextPoint = Math.abs(p[1] - lastPoint[1]);
        const horizontalDistance = Math.abs(p[2] - lastPoint[2]);
        const verticalDistance = Math.abs(p[3] - lastPoint[3]);

        if (distanceXToNextPoint >= minDistance || distanceYToNextPoint >= minDistance || horizontalDistance > 10 || verticalDistance > 10) {
            newValues.push(p);
            lastPoint = p;
        }
    }

    return newValues;
};

const smoothData = (values: Array<number[]>, severity: number) => {
    let newValues = [];
    newValues.push(values[0]);

    for (let i = 1; i < values.length; i++) {
        const start = (i - severity) > 0 ? i - severity : 0;
        const end = (i + severity) < values.length ? i + severity : values.length;
        let sumX = 0;
        let sumY = 0;
        for (let j = start; j < end; j++) {
            sumX += values[j][0];
            sumY += values[j][1];
        }

        const avgX = sumX / (end - start);
        const avgY = sumY / (end - start);

        newValues.push([avgX, avgY, values[i][2], values[i][3]]);
    }

    return newValues;
};

const createChart = (data: Array<number[]>) => {
    const filteredValues = filterData(data, 15);
    const smoothedValues = smoothData(filteredValues, 2);


    // @ts-ignore
    var scatterChart = new Chart(ctx, {
        type: 'scatter',
        data: {
            datasets: [
                {
                    backgroundColor: 'blue',
                    label: `Filtered Dataset ${filteredValues.length}`,
                    data: filteredValues.map(d => {
                        return {
                            'x': 640 - d[0],
                            'y': 640 - d[1]
                        };
                    }),
                    hidden: false
                },
                {
                    backgroundColor: 'red',
                    label: `Smoothed Dataset ${smoothedValues.length}`,
                    data: smoothedValues.map(d => {
                        return {
                            'x': 640 - d[0],
                            'y': 640 - d[1]
                        };
                    }),
                    hidden: true
                },
                {
                    backgroundColor: 'green',
                    label: `Dataset ${data.length}`,
                    data: data.map(d => {
                        return {
                            'x': 640 - d[0],
                            'y': 640 - d[1]
                        };
                    }),
                    hidden: true
                },
            ]
        },
        options: {
            scales: {
                xAxes: [{
                    type: 'linear',
                    position: 'bottom',
                    ticks: {
                        beginAtZero: true,
                        min: 0,
                        max: 640,
                    },
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        min: 0,
                        max: 480,
                    }
                }]
            }
        }
    });
};