const videoStyle = 'transform: rotateY(180deg)';
const canvasStyle = 'position: absolute; left: 0; transform: rotateY(180deg); z-index: 100; ';
const containerStyle = 'position: relative;';
const blurredVideoStyle = 'transform: rotateY(180deg); filter: blur(5px);';
const fixedContainerStyle = 'position: fixed; bottom: 0; left: 0; transform: scale(0.5); transform-origin: left bottom';

export { videoStyle, canvasStyle, containerStyle, blurredVideoStyle, fixedContainerStyle };
