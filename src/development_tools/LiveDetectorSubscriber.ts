import { AbstractDetectorSubscriber } from '../AbstractDetectorSubscriber';
import { Detector } from '../Detector';
import { Hand } from '../types';

export interface TrainingData {
    ys: number,
    xs: Array<number>;
}

class LiveDetectorSubscriber extends AbstractDetectorSubscriber {
    constructor(
        private detector: Detector,
        private trainingData: TrainingData[],
        private poseIndex: number,
        private getAngles: (landmarks: Array<number[]>) => Array<number>,
        private counter: number,
        private collectedData: HTMLSpanElement,
        private maxValue: number
    ) {
        super();
    }

    receiveData = (data: Hand) => {
        this.trainingData.push({ ys: this.poseIndex, xs: this.getAngles(data.landmarks) });
        this.counter++;
        this.collectedData.textContent = `${this.counter}`;
        if (this.counter >= this.maxValue) {
            this.detector.setDetectionFlag(false);
        }
    };
    noData = () => { };

}

export { LiveDetectorSubscriber };