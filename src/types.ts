export interface Hand {
    annotations: HandAnnotations,
    handInViewConfidence: number,
    boundingBox: {
        bottomRight: Array<number>,
        topLeft: Array<number>;
    },
    landmarks: Array<Array<number>>;
}

export interface HandAnnotations {
    indexFinger: Array<Array<number>>,
    middleFinger: Array<Array<number>>,
    palmBase: Array<Array<number>>,
    pinky: Array<Array<number>>,
    ringFinger: Array<Array<number>>,
    thumb: Array<Array<number>>;
}

export interface HandPoseResult {
    pose: number,
    probability: number;
}

export interface NeuralNetworksResult {
    pose: number,
    probability: number;
}