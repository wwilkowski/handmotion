import { Hand } from "./types";

abstract class AbstractDetectorSubscriber {
    abstract receiveData(data: Hand): void;
    abstract noData(): void;
}

export { AbstractDetectorSubscriber };
