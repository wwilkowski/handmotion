import { Logger } from './Logger';
export class MediaDetector {
    constructor() { }
}
MediaDetector.prepareNavigator = () => {
    let defaultNavigator = navigator;
    if (!defaultNavigator.mediaDevices) {
        defaultNavigator.mediaDevices = {};
    }
    if (!defaultNavigator.mediaDevices.getUserMedia) {
        defaultNavigator.mediaDevices.getUserMedia = () => {
            const userMediaGetter = defaultNavigator.webkitGetUserMedia || defaultNavigator.mozGetUserMedia;
            if (!userMediaGetter) {
                return Promise.reject(new Error('getUserMedia method is not implemented in this browser'));
            }
            return new Promise((resolve, reject) => {
                userMediaGetter.call(navigator, resolve, reject);
            });
        };
    }
    Logger.log('Navigator has been prepared. User media has been detected');
    return defaultNavigator;
};
MediaDetector.getVideoStream = async () => {
    const navigator = MediaDetector.prepareNavigator();
    return await navigator.mediaDevices.getUserMedia({ video: true });
};
//# sourceMappingURL=MediaDetector.js.map