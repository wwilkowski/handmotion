import { Detector } from "../Detector";
import { Drawer } from '../Drawer';
import { UILoader } from "../UILoader";
import { VideoLoader } from '../VideoLoader';
import { LiveDetectorSubscriber } from './LiveDetectorSubscriber';

export interface TrainingData {
    ys: number,
    xs: Array<number>;
}

const container = document.querySelector('.videoContainer') as HTMLDivElement;
const video = document.querySelector('.video') as HTMLVideoElement;

const videoLoader = new VideoLoader(video);
videoLoader.load();

const ui = new UILoader(container, video);
const detector = new Detector(video);
const drawer = new Drawer(ui.getCanvas());

detector.loadHandposeModel();
detector.subscribe(drawer);

video.addEventListener('loadeddata', () => {
    detector.detectionCallback();
});

const saveTrainingDataButton = document.querySelector('.saveTrainingDataButton') as HTMLButtonElement;
const getFramesListLive = document.querySelector('.getFramesListLive') as HTMLButtonElement;
const collectedData = document.querySelector('.collectedData') as HTMLSpanElement;

const pointsPairs: Array<Array<number>> = [[0, 2, 4], [0, 6, 8], [0, 10, 12], [0, 14, 16], [0, 18, 20]];
let training_data: TrainingData[] = [];
let dataCounter = 0;

getFramesListLive?.addEventListener('click', () => {
    const poseIndex = parseInt((document.querySelector('.trainingPoseIndex') as HTMLInputElement).value);
    const liveDetectorSubscriber = new LiveDetectorSubscriber(detector, training_data, poseIndex, getAnglesFromLandmarks, dataCounter, collectedData, 1000);
    detector.subscribe(liveDetectorSubscriber);
    detector.setDetectionFlag(true);
});

saveTrainingDataButton?.addEventListener('click', () => {
    const json = JSON.stringify(training_data);
    const blob = new Blob([json], { type: 'application/json' });
    const url = URL.createObjectURL(blob);

    const a = document.createElement('a');
    a.download = 'data.json';
    a.href = url;

    a.click();
});

const getAnglesFromLandmarks = (landmarks: Array<Array<number>>) => {
    const angles: Array<number> = [];
    pointsPairs.forEach((point: Array<number>) => {
        const a = [landmarks[point[1]][0] - landmarks[point[0]][0], landmarks[point[1]][1] - landmarks[point[0]][1]];
        const b = [landmarks[point[2]][0] - landmarks[point[1]][0], landmarks[point[2]][1] - landmarks[point[1]][1]];
        angles.push((a[0] * b[0] + a[1] * b[1]) / (Math.sqrt(a[0] ** 2 + a[1] ** 2) * (Math.sqrt(b[0] ** 2 + b[1] ** 2))));
    });
    return angles;
};

