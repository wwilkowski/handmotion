import poseWorker from "./poseWorker";
import { HandPoseResult } from '../types';


class HandPositionNetwork {
    private worker: Worker;
    private pending: Boolean;
    private lastPosition: number | null;

    constructor(callback: (result: HandPoseResult) => void) {
        this.worker = new Worker(poseWorker);
        this.lastPosition = null;

        this.worker.onmessage = (event) => {
            if (event.data && event.data.log) {
                // console.log(event.data.log);
            }
            else {
                this.pending = false;
                if (event.data) {
                    if (!this.lastPosition || event.data.pose !== this.lastPosition) {
                        this.lastPosition = event.data.pose;
                        callback(event.data);
                    }
                }
            }
        };

        this.pending = false;
    }

    predict = async (landmarks: Array<number>) => {
        if (!this.pending) {
            this.pending = true;
            this.worker.postMessage({
                input: landmarks
            });
        }
    };
}

export { HandPositionNetwork };
