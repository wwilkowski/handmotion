import { videoStyle } from "./constants/styles";
import { Logger } from "./Logger";
import { MediaDetector } from "./MediaDetector";
export class VideoLoader {
    constructor(video) {
        this.load = async () => {
            return new Promise(async (resolve, reject) => {
                this.video.srcObject = await MediaDetector.getVideoStream();
                this.video.addEventListener('loadeddata', () => {
                    Logger.log('Stream has been added to video');
                    Logger.log('Video data has been loaded');
                    this.video.setAttribute('style', videoStyle);
                    this.video.play();
                    resolve();
                });
            });
        };
        this.play = () => {
            this.video.addEventListener('loadedmetadata', () => {
                this.video.play();
                Logger.log('Video meta data have been loaded. Start video...');
            });
        };
        this.video = video;
    }
}
//# sourceMappingURL=VideoLoader.js.map