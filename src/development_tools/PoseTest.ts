import { Detector } from '../Detector';
import { Drawer } from '../Drawer';
import { HandPositionConsumer } from '../HandPositionConsumer';
import { HandPositionNetwork } from '../handPositionNetwork/HandPositionNetwork';
import { NeuralNetworksResult } from '../types';
import { UILoader } from '../UILoader';
import { VideoLoader } from '../VideoLoader';

const pose = document.querySelector('.pose') as HTMLSpanElement;

const onHandPosition = (value: NeuralNetworksResult) => {
    const labels = ['Zamknięta dłoń', 'Jeden palec', 'Dwa palce', 'Trzy palce', "Cztery palce", "Otwarta dłoń"];
    if (value) {
        if (value.probability >= 0.9) {
            pose.textContent = `${labels[value.pose]} z prawdopodobienstwem ${value.probability}`;
        }
    };
};

const container = document.querySelector('.videoContainer') as HTMLDivElement;
const video = document.querySelector('.video') as HTMLVideoElement;
const trainButton = document.querySelector('.trainButton');

const videoLoader = new VideoLoader(video);
videoLoader.load();

const ui = new UILoader(container, video);
const detector = new Detector(video);
const drawer = new Drawer(ui.getCanvas());
const handPositionNetwork = new HandPositionNetwork(onHandPosition);;
const handPositionConsumer = new HandPositionConsumer(handPositionNetwork);

detector.loadHandposeModel();
detector.subscribe(drawer);
detector.subscribe(handPositionConsumer);
video.addEventListener('loadeddata', () => {
    detector.detectionCallback();
});

trainButton?.addEventListener('click', async () => {
    detector.setDetectionFlag(true);
});