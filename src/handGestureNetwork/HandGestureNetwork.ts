import { Hand, HandPoseResult } from '../types';
import gestureWorker from "./gestureWorker";

class HandGestureNetwork {
    private worker: Worker;
    private pending: Boolean;

    constructor(callback: (result: HandPoseResult) => void) {
        this.worker = new Worker(gestureWorker);

        this.worker.onmessage = (event) => {
            if (event.data && event.data.log) {
                // console.log(event.data.log);
            }
            else {
                this.pending = false;
                if (event.data) {
                    callback(event.data);
                }
            }
        };

        this.pending = false;
    }

    predict = async (input: Array<number>) => {
        if (!this.pending) {
            this.pending = true;
            this.worker.postMessage({
                input: input
            });
        }
    };

    clear = () => {
        this.worker.postMessage({
            clear: true
        });
    };
}

export { HandGestureNetwork };
