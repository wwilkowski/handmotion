import json
import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from tensorflow.keras.utils import to_categorical
import tensorflowjs as tfjs

if __name__ == '__main__':
    with open("pose_data/pose_data.json") as f:
        data = json.load(f)
        X = []
        Y = []
        for i in range(len(data)):
            X.append(data[i]["xs"])
            Y.append(data[i]["ys"])
    X = np.array(X)
    Y = np.array(Y)
    Y = to_categorical(Y, 6)

    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2)

    model = Sequential()
    model.add(Dense(128, activation='relu', input_shape=([5])))
    model.add(Dense(64, activation='relu'))
    model.add(Dense(32, activation='relu'))
    model.add(Dense(6, activation='softmax'))

    model.summary()
    print("LICZBA DANYCH: ", len(X_train))
    print("LICZBA DANYCH: ", len(X_test))
    adam = keras.optimizers.Adam(lr=0.001)
    model.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])

    no_of_epochs = 10
    history = model.fit(X_train, Y_train, epochs=no_of_epochs, validation_data=(X_test, Y_test))

    loss_train = history.history['loss']
    epochs = range(1, no_of_epochs+1)
    plt.plot(epochs, loss_train, 'g', label='Training loss')
    plt.title('Training loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.savefig("training_loss.png")

    original_labels = keras.backend.argmax(Y_test)
    score = keras.backend.argmax(model.predict(X_test))

    fig, ax = plt.subplots()
    plot_data = confusion_matrix(original_labels, score)
    ax.matshow(plot_data, cmap=plt.cm.Blues, vmax=len(X_test)/6)

    for (i, j), z in np.ndenumerate(plot_data):
        ax.text(j, i, '{:0.0f}'.format(z), ha='center', va='center')

    plt.savefig("testdata.png")
    model.save("Keras-20-epochs")
    tfjs.converters.save_keras_model(model, 'tfjsmodel')
