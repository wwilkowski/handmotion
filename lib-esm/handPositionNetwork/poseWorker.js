export default URL.createObjectURL(new Blob([
    '(',
    function () {
        importScripts("https://cdn.jsdelivr.net/npm/@tensorflow/tfjs");
        const ctx = self;
        let model;
        const loadModel = async () => {
            model = await tf.loadLayersModel('http://wwilkowski.com/tfjsmodel/model.json');
        };
        loadModel();
        ctx.addEventListener("message", async (event) => {
            if (model) {
                const { input } = event.data;
                const tensor = tf.tensor([input.flat()]);
                const prediction = model.predict(tensor);
                const winnerIndex = Array.from(prediction.argMax(-1).dataSync())[0];
                const probability = Array.from(prediction.dataSync())[winnerIndex];
                if (probability >= 0.9) {
                    ctx.postMessage({
                        'pose': winnerIndex,
                        'probability': probability
                    });
                }
                else {
                    ctx.postMessage(null);
                }
            }
            else {
                ctx.postMessage(null);
            }
        });
    }.toString(),
    ')()',
], { type: 'application/javascript' }));
//# sourceMappingURL=poseWorker.js.map