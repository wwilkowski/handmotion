export class Logger {
    constructor() { }

    static log = (text: string) => {
        console.info(`[HANDMOTION LOG] ${text}`);
    };
}