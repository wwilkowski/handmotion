import { blurredVideoStyle, canvasStyle, containerStyle, videoStyle, fixedContainerStyle } from './constants/styles';
import { setElementAttribute } from './helpers';
export class UILoader {
    constructor(container, video, blurred = true, fixed = false) {
        this.width = 640;
        this.height = 480;
        this.initVideoProperties = () => {
            setElementAttribute(this.video, 'width', `${this.width}`);
            setElementAttribute(this.video, 'height', `${this.height}`);
            setElementAttribute(this.video, 'style', this.blurred ? blurredVideoStyle : videoStyle);
        };
        this.createCanvasElement = () => {
            const canvas = document.createElement('canvas');
            setElementAttribute(canvas, 'width', `${this.width}`);
            setElementAttribute(canvas, 'height', `${this.height}`);
            setElementAttribute(canvas, 'style', canvasStyle);
            return canvas;
        };
        this.appendCanvasToDocument = (container, canvas) => {
            setElementAttribute(container, 'style', this.fixed ? fixedContainerStyle : containerStyle);
            container.appendChild(canvas);
        };
        this.getCanvas = () => this.canvas;
        this.video = video;
        this.blurred = blurred;
        this.fixed = fixed;
        this.initVideoProperties();
        this.canvas = this.createCanvasElement();
        this.appendCanvasToDocument(container, this.canvas);
    }
}
//# sourceMappingURL=UILoader.js.map