const path = require('path');

module.exports = {
    mode: 'development',
    devtool: 'inline-source-map',
    entry: {
        'handmotion': './src/index.ts',
        'GestureTest': './src/development_tools/GestureTest.ts',
        'getGestures': './src/development_tools/getGestures.ts',
        'frameDetection': './src/development_tools/FramesDataGenerator.ts',
        'poseTest': './src/development_tools/PoseTest.ts',
        'gestureTest': './src/development_tools/GestureTest.ts'
    },
    module: {
        rules: [
            {
                test: /\.worker\.js$/,
                use: {
                    loader: "worker-loader",
                },
            },
            {
                test: /\.ts$/,
                use: 'ts-loader',
                exclude: [/node_modules/],
            },
        ]
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js']
    },
    output: {
        path: path.resolve(__dirname, 'lib'),
        libraryTarget: 'umd',
        library: 'handmotion',
        umdNamedDefine: true
    }
};