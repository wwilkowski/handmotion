import { AbstractDetectorSubscriber } from "./AbstractDetectorSubscriber";
import { Hand } from "./types";

export class Drawer extends AbstractDetectorSubscriber {
    private canvas: HTMLCanvasElement;
    private ctx: CanvasRenderingContext2D;
    constructor(canvas: HTMLCanvasElement) {
        super();
        this.canvas = canvas;
        this.ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
        this.ctx.fillStyle = '#FF0000';
        this.ctx.strokeStyle = '#FF0000';
    }

    receiveData = (data: Hand) => {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.drawLandmarks(data.landmarks);
        this.drawLine(data.landmarks);
    };

    noData = () => {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    };

    drawLandmarks = (landmarks: Array<number[]>) => {
        landmarks.forEach(landmark => {
            this.drawPoint(landmark[0], landmark[1]);
        });
    };

    drawPoint = (x: number, y: number) => {
        this.ctx.beginPath();
        this.ctx.arc(x - 3, y - 3, 6, 0, 2 * Math.PI);
        this.ctx.fill();
    };

    drawLine = (points: Array<number[]>) => {
        const region = new Path2D();
        for (let i = 1; i < points.length; i += 4) {
            region.moveTo(points[0][0], points[0][1]);
            for (let j = i; j < i + 4; j++) {
                const point = points[j];
                region.lineTo(point[0], point[1]);
            }
        }
        this.ctx.stroke(region);
    };
}