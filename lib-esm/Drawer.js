import { AbstractDetectorSubscriber } from "./AbstractDetectorSubscriber";
export class Drawer extends AbstractDetectorSubscriber {
    constructor(canvas) {
        super();
        this.receiveData = (data) => {
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.drawLandmarks(data.landmarks);
            this.drawLine(data.landmarks);
        };
        this.noData = () => {
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        };
        this.drawLandmarks = (landmarks) => {
            landmarks.forEach(landmark => {
                this.drawPoint(landmark[0], landmark[1]);
            });
        };
        this.drawPoint = (x, y) => {
            this.ctx.beginPath();
            this.ctx.arc(x - 3, y - 3, 6, 0, 2 * Math.PI);
            this.ctx.fill();
        };
        this.drawLine = (points) => {
            const region = new Path2D();
            for (let i = 1; i < points.length; i += 4) {
                region.moveTo(points[0][0], points[0][1]);
                for (let j = i; j < i + 4; j++) {
                    const point = points[j];
                    region.lineTo(point[0], point[1]);
                }
            }
            this.ctx.stroke(region);
        };
        this.canvas = canvas;
        this.ctx = canvas.getContext('2d');
        this.ctx.fillStyle = '#FF0000';
        this.ctx.strokeStyle = '#FF0000';
    }
}
//# sourceMappingURL=Drawer.js.map