## Instalacja zależności:

`npm install`

## Budowa plików projektu oraz iruchomienie środowiska developerskiego do testów:

```
npm run build
npm run dev
```

## Narzędzie do zbierania danych treningowych dla sieci do klasyfikacji pozycji dłoni

http://localhost:8080/public/index.html

## Narzędzie do zbierania danych treningowych dla sieci do klasyfikacji gestów dłoni

http://localhost:8080/public/getGestures.html

## Narzędzie do przetestowania utworzonego modelu klasyfikacji pozycji

http://localhost:8080/public/poseTest.html

## Narzędzie do przetestowania utworzonego modelu klasyfikacji gestów

http://localhost:8080/public/gestureTest.html