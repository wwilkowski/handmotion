// @ts-nocheck
import { LayersModel } from '@tensorflow/tfjs';

export default URL.createObjectURL(
    new Blob([
        '(',
        function () {
            const smoothData = (values: Array<number[]>, severity: number) => {
                let newValues = [];
                newValues.push(values[0]);

                for (let i = 1; i < values.length; i++) {
                    const start = (i - severity) > 0 ? i - severity : 0;
                    const end = (i + severity) < values.length ? i + severity : values.length;
                    let sumX = 0;
                    let sumY = 0;
                    for (let j = start; j < end; j++) {
                        sumX += values[j][0];
                        sumY += values[j][1];
                    }

                    const avgX = sumX / (end - start);
                    const avgY = sumY / (end - start);

                    newValues.push([avgX, avgY, values[i][2]]);
                }

                return newValues;
            };

            const normalizeData = (values: Array<number[]>) => {
                let maxX = 0;
                let maxY = 0;
                let maxVerticalDistance = 0;

                for (let j = 0; j < values.length; j++) {
                    if (maxX < values[j][0]) {
                        maxX = values[j][0];
                    }
                    if (maxY < values[j][1]) {
                        maxY = values[j][1];
                    }
                    if (maxVerticalDistance < values[j][2]) {
                        maxVerticalDistance = values[j][2];
                    }
                }

                return values.map(value => [value[0] / maxX, value[1] / maxY, value[2] / maxVerticalDistance]);
            };

            const checkPointsDistance = (lastPoint: number[] | null, newPoint: number[]) => {
                if (!lastPoint) {
                    return true;
                }

                const distanceXToNextPoint = Math.abs(newPoint[0] - lastPoint[0]);
                const distanceYToNextPoint = Math.abs(newPoint[1] - lastPoint[1]);
                const verticalDistance = Math.abs(newPoint[2] - lastPoint[2]);

                if (distanceXToNextPoint >= 15 || distanceYToNextPoint >= 15 || verticalDistance > 10) {
                    return true;
                }

                return false;
            };


            importScripts("https://cdn.jsdelivr.net/npm/@tensorflow/tfjs");
            const ctx: Worker = self as any;
            let queue: Array<Array<number>> = [];
            let model: LayersModel | null;
            let guide: Boolean = false;

            const loadModel = async () => {
                model = await tf.loadLayersModel('http://wwilkowski.com/tfjsmodel2/model.json');
            };

            loadModel();

            let timeout: ReturnType<typeof setTimeout>;
            let lastInput: number[] | null;

            ctx.addEventListener("message", (event) => {
                if (event.data.clear) {
                    queue = [];
                    return;
                }

                const { input } = event.data;

                if (checkPointsDistance(lastInput, input)) {
                    if (queue.length > 15) {
                        queue.shift();
                    }

                    clearTimeout(timeout);
                    lastInput = input;
                    if (!guide) {
                        queue.push(input);
                    }
                }

                if (queue.length >= 3 && !guide && model) {
                    const finalQueue = normalizeData(smoothData(queue, 2));
                    let testTensor = tf.tensor(finalQueue);
                    testTensor = testTensor.reshape([1, testTensor.shape[0], testTensor.shape[1]]);
                    const prediction = (model.predict(testTensor) as tf.Tensor);
                    const winnerIndex = Array.from(prediction.argMax(-1).dataSync())[0] as number;
                    const probability = Array.from(prediction.dataSync())[winnerIndex];

                    if (probability >= 0.9) {
                        queue = [];
                        ctx.postMessage({
                            'pose': winnerIndex,
                            'probability': probability
                        });
                        guide = true;

                        setTimeout(() => {
                            guide = false;
                        }, 500);
                    }
                    else {
                        ctx.postMessage(null);
                    }
                }
                else {
                    ctx.postMessage(null);
                }

                timeout = setTimeout(() => {
                    queue = [];
                }, 1000);
            });
        }.toString(),
        ')()',],
        { type: 'application/javascript' }
    )
);


